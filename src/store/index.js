import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    activeTodos: [],
    doneTodos: [],
    deletedTodos: []
  },
  mutations: {
    ADD_TODO: (state, todo) => {
      state.activeTodos.unshift(todo)
    },
    UPDATE_ACTIVE_TODOS: (state, todos) => {
      state.activeTodos = todos
    },
    ADD_DONE_TODO: (state, todo) => {
      state.doneTodos.unshift(todo)
    },
    ADD_REMOVED_TODO: (state, todo) => {
      state.deletedTodos.unshift(todo)
    }
  },
  actions: {
    UPDATE_TODO ({ commit, getters }, todo) {
      const allTodos = [...getters.ACTIVE_TODOS]
      allTodos.splice(todo.index, 1)
      switch (todo.action) {
        case 'done':
          const doneTodo = { ...todo.todoInfo }
          commit('ADD_DONE_TODO', doneTodo)
          break
        case 'remove':
          const removedTodo = { ...todo.todoInfo }
          commit('ADD_REMOVED_TODO', removedTodo)
          break
      }
      commit('UPDATE_ACTIVE_TODOS', allTodos)
    }
  },
  getters: {
    ACTIVE_TODOS (state) {
      return state.activeTodos
    },
    DONE_TODOS (state) {
      return state.doneTodos
    },
    REMOVED_TODOS (state) {
      return state.deletedTodos
    }
  }
})
