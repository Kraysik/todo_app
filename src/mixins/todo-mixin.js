import moment from 'moment'

export const todoMixin = {
  methods: {
    getDate (date) {
      return moment(date).format('DD-MM-YYYY')
    }
  }
}
